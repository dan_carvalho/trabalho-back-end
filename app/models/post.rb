class Post < ApplicationRecord
  ##Relations
  belongs_to :user
  has_many :like_posts
  has_many :users, through: :like_posts
  has_many :comments, dependent: :destroy
  ##Validation
  validates :content_text, presence: true,  length: {maximum: 250}
  ##Functions
end
