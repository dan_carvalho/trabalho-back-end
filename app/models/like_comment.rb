class LikeComment < ApplicationRecord
  #references
  belongs_to :comment
  belongs_to :user
  ##Validation
  validate :already_liked?
  ##Functions
  def already_liked?
    if comment.users.include?(user)
      errors.add(:already_liked,"Voce ja deu like nesse comentario")
    end
  end 
end
