class Comment < ApplicationRecord
  ##Relations
  belongs_to :post
  belongs_to :user
  has_many :replies, class_name: "Comment", foreign_key: "parent_comment_id"
  belongs_to :parent_comment, class_name: "Comment", optional: true
  ##Validation
  validates :content_text, presence: true,  length: {maximum: 250}
  validate :anti_spam
  ##Functions  
  def anti_spam
    if Comment.where(content_text: content_text, user_id: user_id).length > 3
      errors.add(:span,"Voce nao pode mandar essa msg novamente")
    end
  end
end
