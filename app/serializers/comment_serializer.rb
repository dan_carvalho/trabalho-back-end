class CommentSerializer < ActiveModel::Serializer
  attributes :id, :content_text
  has_one :post
end
