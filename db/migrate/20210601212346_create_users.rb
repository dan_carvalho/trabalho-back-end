class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :name
      t.date :birthday
      t.string :email
      t.integer :gender
      t.string :password_digest
      t.string :nickname

      t.timestamps
    end
  end
end
